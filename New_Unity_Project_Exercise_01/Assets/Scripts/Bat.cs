﻿namespace CoAHomework
{
    public class Bat : Enemy
    {
        public int eyes = 2;
        public int wings = 2;
        public float width = 3.5f;
        public float mana = 10.5f;
        public string spellone = "Abracadabra";
        public string spelltwo = "Leviosaa";
        public string spellthree = "supercalifragilistic";
        public bool isDismembered;
        public bool areAllEyesOpen = true;
        public bool isInRange;
        public bool isFlying = true;
    }
}