﻿namespace CoAHomework
{
    public class AggressiveEntity : Enemy
    {
        public int health = 10;
        public int level = 1;
        public float speed = 5.5f;
        public float aggressionLevel = 3.5f;
        public string name = "Spider";
        public string declaration = "You want to fight?";
        public string greeting;
        public bool isAlive = true;
        public bool isAgressive = true;
        public bool nearPlayer;
        public bool nearEntity;
    }
}