﻿namespace CoAHomework
{
    public class Ork : Enemy
    {
        public int strength = 5;
        public int dexterity;
        public float height = 10.5f;
        public float speed = 0.7f;
        public string warCry = "UGAUGA";
        public string name = "Mosreath";
        public string defeatDeclaration = "Never Surrender";
        public bool eyesOpen = true;
        public bool hasWeapon = true;
        public bool tilted = true;
        public bool awake;
    }
}