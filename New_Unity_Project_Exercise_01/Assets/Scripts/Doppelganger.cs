﻿namespace CoAHomework
{
    public class Doppelganger : Enemy
    {
        public int energy = 10;
        public int expPoints = 50;
        public float positionXScale = 5.5f;
        public float positionYScale;
        public string title = "King of the Underworld";
        public string contidion = "blinded";
        public string conditionTwo = "rooted";
        public bool hasShadow;
        public bool undyingRage;
        public bool surrounded;
        public bool isEvil = true;
    }
}